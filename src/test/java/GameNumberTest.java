import fizzbuzz.GameNumber;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author wuql
 * @desc
 * @date 2020/10/9
 */
public class GameNumberTest {
    @Test
    public void should_create_game_number_from_raw_number(){
        GameNumber gameNumber = new GameNumber(1);
    }

    @Test
    public void should_say_1_when_raw_number_is_1(){
        GameNumber gameNumber = new GameNumber(1);
        assertThat(gameNumber.toString(),is("1"));
    }

    @Test
    public void should_say_fizz_when_raw_number_is_3(){
        GameNumber gameNumber = new GameNumber(3);
        assertThat(gameNumber.toString(),is("Fizz"));
    }

    @Test
    public void should_say_buzz_when_raw_number_is_5(){
        GameNumber gameNumber = new GameNumber(5);
        assertThat(gameNumber.toString(),is("Buzz"));
    }

    @Test
    public void should_say_fizzbuzz_when_raw_number_is_15(){
        assertThat(new GameNumber(15).toString(),is("FizzBuzz"));
        assertThat(new GameNumber(51).toString(),is("FizzBuzz"));
    }

    @Test
    public void should_say_fizz_when_raw_number_contains_3(){
        GameNumber gameNumber = new GameNumber(13);
        assertThat(gameNumber.toString(),is("Fizz"));
    }

    @Test
    public void should_say_buzz_when_raw_number_contains_5(){
        GameNumber gameNumber = new GameNumber(52);
        assertThat(gameNumber.toString(),is("Buzz"));
    }

    @Test
    public void should_say_buzz_when_raw_number_contains_3_and_5(){
        assertThat(new GameNumber(53).toString(),is("FizzBuzz"));
    }
}
