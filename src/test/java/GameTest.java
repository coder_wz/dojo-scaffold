import fizzbuzz.Game;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author wuql
 * @desc
 * @date 2020/10/10
 */
public class GameTest {
    @Test
    public void should_create_game_object_with_given_size(){
        Game game = new Game(100);
        assertThat(game.size(),is(100));
    }

    @Test
    public void should_provide_words_to_be_spoken(){
        Game game = new Game(100);
        List<String> words = game.words();
        assertThat(words.size(),is(100));
        assertThat(words.get(0),is("1"));
        assertThat(words.get(2),is("Fizz"));
        assertThat(words.get(4),is("Buzz"));
        assertThat(words.get(12),is("Fizz"));
        assertThat(words.get(14),is("FizzBuzz"));
        assertThat(words.get(51),is("Buzz"));
        assertThat(words.get(52),is("FizzBuzz"));
    }
}
