package fizzbuzz;


import static java.lang.String.valueOf;

/**
 * @author wuql
 * @desc
 * @date 2020/10/9
 */
public class GameNumber {
    private  int rawNumber;
    private final static int NUMBER_3 =3;
    private final static int NUMBER_5 =5;
    private final static String  FIZZ ="Fizz";
    private final static String  BUZZ ="Buzz";

    public GameNumber(int rawNumber) {
        this.rawNumber =rawNumber;
    }

    @Override
    public String toString() {
        if (isRelatedTo(NUMBER_3) && isRelatedTo(NUMBER_5)){
            return FIZZ+BUZZ;
        }
        if(isRelatedTo(NUMBER_3)){
            return FIZZ;
        }
        if (isRelatedTo(NUMBER_5)){
            return BUZZ;
        }
        return valueOf(rawNumber);
    }

    private boolean isRelatedTo(int denominator) {
        return rawNumber % denominator == 0 || valueOf(rawNumber).contains(valueOf(denominator));
    }
}
