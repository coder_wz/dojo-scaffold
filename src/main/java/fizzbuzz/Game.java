package fizzbuzz;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wuql
 * @desc
 * @date 2020/10/10
 */
public class Game {
    private List<GameNumber> gameNumbers;

    public Game(int size) {
        gameNumbers =new ArrayList<>();
        for (int i=1;i<=size;i++){
            gameNumbers.add(new GameNumber(i));
        }
    }

    public int size() {
        return gameNumbers.size();
    }

    public List<String> words() {
        return gameNumbers.stream().map(GameNumber::toString).collect(Collectors.toList());
    }
}
